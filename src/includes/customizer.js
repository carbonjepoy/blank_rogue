/*Customizer Preview*/

( function( $ ) {

    wp.customize( "header-background", function( value ) {
        value.bind( function( newval ) {
            $( ".header" ).css( "background-color", newval ); 
        } );
    });

    wp.customize( "footer-background", function( value ) {
        value.bind( function( newval ) {
            $( ".footer" ).css( "background-color", newval );
        } );
    });

    wp.customize( "header-color", function( value ) {
        value.bind( function( newval ) {
            $( "h1, h2, h3, h4, h5" ).css( "color", newval );
        } );
    });

    wp.customize( "text-color", function( value ) {
        value.bind( function( newval ) {
            $( "p" ).css( "color", newval );
        } );
    });

    wp.customize( "link-color", function( value ) {
        value.bind( function( newval ) {
            $( "a" ).css( "color", newval );
        } );
    });

    wp.customize( "button-color", function( value ) {
        value.bind( function( newval ) {
            $( "button" ).css( "background-color", newval );
        } );
    });

    wp.customize( "button_text-color", function( value ) {
        value.bind( function( newval ) {
            $( "button" ).css( "color", newval );
        } );
    });

    wp.customize( "copyright_text", function( value ) {
        value.bind( function( newval ) {
            $( ".copyright" ).html( newval );
        } );
    });
    
} )( jQuery );