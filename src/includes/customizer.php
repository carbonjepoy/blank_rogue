<?php 

	//Customizer 
	function rogue_customizer_register($wp_customize){ 

		/*---HEADER SECTION---*/
		$wp_customize->add_section('header_settings_section', array(
	    	'title'          => 'Header'
	     ));

		//Background Color Setting
	    $wp_customize->add_setting('header-background', array(
	    	'default'     => '#f2f2f2',
	    	'transport'   => 'postMessage',
	    ));

	    //Background Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header-background', array(
	    	'label'     => 'Header Background Color Setting',
	    	'section'   => 'header_settings_section',
	    	'settings'  => 'header-background',
	    )));
	    /*---END HEADER SECTION---*/

	    /*---FOOTER SECTION---*/
	    $wp_customize->add_section('footer_settings_section', array(
	    	'title'          => 'Footer'
	     ));

	    //Background Color Setting
	    $wp_customize->add_setting('footer-background', array(
	    	'default'     => '#f2f2f2',
	    	'transport'   => 'postMessage',
	    ));

	    //Background Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer-background', array(
	    	'label'     => 'Footer Color Setting',
	    	'section'   => 'footer_settings_section',
	    	'settings'  => 'footer-background',
	    )));

	    //Copyright Text Setting
	    $wp_customize->add_setting('copyright_text', array(
	    	'default'   => 'Default Text For Footer Section',
	    	'transport'   => 'postMessage',
	    ));

	    //Copyright Text Control
	    $wp_customize->add_control('copyright_text', array(
		    'label'   	=> 'Copyright Text',
		    'section' 	=> 'footer_settings_section',
		    'type'    	=> 'textarea'
	    ));

	    /*---END FOOTER SECTION---*/

	    /*---COLORS SECTION---*/

	    //Headers Color Setting
	    $wp_customize->add_setting('header-color', array(
		   	'default'     => '#333333',
		   	'transport'   => 'postMessage',
		));

	    //Headers Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header-color', array(
		    'label'     => 'J2 Header Color Setting',
		    'section'   => 'colors',
		    'settings'  => 'header-color',
		)));

		//Links Color Setting
	    $wp_customize->add_setting('link-color', array(
		   	'default'     => '#0070c9',
		   	'transport'   => 'postMessage',
		));

	    //Links Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link-color', array(
		    'label'     => 'Link Color Setting',
		    'section'   => 'colors',
		    'settings'  => 'link-color',
		    'priority' => 1,
		)));

		//Text Color Setting
		$wp_customize->add_setting('text-color', array(
		   	'default'     => '#333333',
		   	'transport'   => 'postMessage',
		));

		//Text Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text-color', array(
		    'label'     => 'Text Color Setting',
		    'section'   => 'colors',
		    'settings'  => 'text-color',
		)));

		//Buttons Color Setting
	    $wp_customize->add_setting('button-color', array(
		   	'default'     => '#238adb',
		   	'transport'   => 'postMessage',
		));

	    //Buttons Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button-color', array(
		    'label'     => 'Button Color Setting',
		    'section'   => 'colors',
		    'settings'  => 'button-color',
		)));

		//Buttons Color Setting
	    $wp_customize->add_setting('button_text-color', array(
		   	'default'     => '#ffffff',
		   	'transport'   => 'postMessage',
		));

	    //Buttons Color Control
	    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_text-color', array(
		    'label'     => 'Button Text Color Setting',
		    'section'   => 'colors',
		    'settings'  => 'button_text-color',
		)));

	    /*---END COLORS SECTION---*/

	};	

	//Customizer JS
	function blank_rogue_customizerjs() {
		wp_enqueue_script( 
			'rogue_customizer', 
			get_template_directory_uri() . '/includes/customizer.js', 
			array('jquery', 'customize-preview' ), 
			'', 
			true 
		);
	}

	/*ACTION HOOKS*/
	add_action('customize_register', 'rogue_customizer_register'); //Customize function register
	add_action( 'customize_preview_init', 'blank_rogue_customizerjs' ); //Customizer preview JS register

?>
