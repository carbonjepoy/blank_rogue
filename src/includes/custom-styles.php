<?php

	function rogue_custom_styles($custom_css) {

		$custom_css = '';

		//Header Background
	    $header_bgcolor = get_theme_mod( 'header-background', '#f2f2f2' ); 
	    $custom_css .= " header { background: {$header_bgcolor};}" . "\n";

		//Footer Background
	    $footer_color = get_theme_mod( 'footer-background', '#f2f2f2' ); 
	    $custom_css .= " .footer { background: {$footer_color};}" . "\n";
	    
	    //Header Color
	    $header_color = get_theme_mod( 'header-color', '#333333' ); 
	    $custom_css .= " h1, h2, h3, h4, h5 { color: {$header_color};}" . "\n";

	    //Text-color
	    $texts_color = get_theme_mod( 'text-color', '#333333' ); 
	    $custom_css .= " p { color: {$texts_color};}" . "\n";

	    //Link-color
	    $links_color = get_theme_mod( 'link-color', '#0070c9' ); 
	    $custom_css .= " a { color: {$links_color};}" . "\n";

	    //Button-color
	    $buttons_color = get_theme_mod( 'button-color', '#238adb' ); 
	    $custom_css .= " button { background: {$buttons_color};}" . "\n";

	    //Button_Text-color
	    $buttons_text_color = get_theme_mod( 'button_text-color', '#ffffff' ); 
	    $custom_css .= " button { color: {$buttons_text_color};}" . "\n";

	    

	    wp_add_inline_style( 'rogue-style', $custom_css );
	}

	/*ACTION HOOKS*/
	add_action( 'wp_enqueue_scripts', 'rogue_custom_styles' );//Inline custom style

?>