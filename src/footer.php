			

		</div>
		<!-- /wrapper -->

		<!-- footer -->
			<footer class="footer" role="contentinfo">

				<!-- logo -->
				<div class="footer-logo">
					<a href="<?php echo home_url(); ?>">
						<?php /*the_custom_logo();*/ ?>
					</a>
				</div>
				<!-- /logo -->

				<!-- footer nav -->
				<?php footer_nav(); ?>
				<!-- /footer nav -->

				<!-- copyright -->
				<?php 
					$copyright = get_theme_mod('copyright_text');
				?>
				<p class="copyright">
					<?php echo $copyright;?>
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
